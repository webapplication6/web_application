import React from "react";
import './Errors.css'
import "../components/3.css";
import { useFormik } from "formik";
import Swal from "sweetalert2";
import { signUpUser } from "../schemas/SignUpCustomer";
import { Navigate, useNavigate } from "react-router-dom";


const initialValues = {

  firstname: "",
  email: "",
  password: "",
  lastname: "",
  conformpassword:"" 

}

export default function CustomerRegister() {





  return (
    <div>
      <CustomerTable />
    </div>
  );
}

function CustomerTable() {


const navigate=useNavigate();

  const { values, errors, touched, handleChange, handleBlur, handleSubmit } = useFormik({

    initialValues: initialValues,
    validationSchema: signUpUser,
    onSubmit: (value) => {
      console.log(value)
      Swal.fire({
        position: "center",
        icon: "success",
        title: "Welcome ",
        showConfirmButton: false,
        timer: 1500,
      });
      navigate("/registerdone");
    
    }


  });
  console.log(errors);
  return (
    <div className="container mt-">
      <div className="title">User SignUp Form</div>
  
      <form onSubmit={handleSubmit}>
        <div className="user-details">
          <div className="input-box">
            <span className="details">First Name
              {errors.name && touched.name ? (<span className="errors">{errors.name}
              </span>) : null}
            </span>
            <input
              type="text"
              placeholder="Enter your name"
              id="firstName"
              name="name"
              value={values.name}
              onChange={handleChange}
              onBlur={handleBlur}
            />
          </div>
          <div className="input-box">
            <span className="details">Last Name
              {errors.address && touched.address ? (<span className="errors">{errors.address}
              </span>) : null}
            </span>
            <input
              type="text"
              placeholder="Enter your address"
              id="emailid"
              name="name1"
              value={values.address}
              onChange={handleChange}
              onBlur={handleBlur}
            />
          </div>
          

          <div className="input-box">
            <span className="details">Email
              {errors.email && touched.email ? (<span className="errors">{errors.email}
              </span>) : null}
            </span>
            <input
              type="email"
              placeholder="Enter your email"
              id="emailid"
              name="email"
              value={values.email}
              onChange={handleChange}
              onBlur={handleBlur}
            />
          </div>
          <div className="input-box">
            <span className="details">Password
              {errors.password && touched.password ? (<span className="errors">{errors.password}
              </span>) : null}
            </span>
            <input
              type="password"
              placeholder="Enter your password"
              id="password"
              name="password"
              value={values.password}
              onChange={handleChange}
              onBlur={handleBlur}
            />
          </div>
          <div className="input-box">
            <span className="details">Conform Password
              {errors.uid && touched.uid ? (<span className="errors">{errors.uid}
              </span>) : null}
            </span>
            <input
              type="number"
              placeholder="Enter your adhar number"
              name="uid"
              value={values.uid}
              onChange={handleChange}
              onBlur={handleBlur}
            />
          </div>
                  </div>
        <div className="button">
          <input type="submit" className="bg-success bg-gradient" value="Submit" />
        </div>
      </form>
    </div>
  );
}
