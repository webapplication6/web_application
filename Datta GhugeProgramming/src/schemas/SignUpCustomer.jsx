import * as Yup from 'yup'



export const  signUpUser =Yup.object({

    name :Yup.string().min(2).max(15).matches(
        /^([A-Za-z\u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff\s]*)$/gi,
            '  Name can only contain Latin letters.'
        ).required("     please fill this field"),
    email :Yup.string().email().required("    please fill this field"),
    password: Yup.string().trim().min(6).max(20).required("enter  password"),
   
    conformpassword: Yup.string().min(6).max(20).required(" password").oneOf([Yup.ref("password"), null], "  password must Match"),
     uidPic :Yup.string().required("   please upload in this  field"),
     name1 :Yup.string().required("   please fill this field"),
     
})